package com.deep6.uima.annotators;

import com.deep6.uima.annotations.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.util.Version;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.OperationalProperties;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.tartarus.snowball.ext.PorterStemmer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;

/**
 * UIMA annotator that tokenizes documents using the Lucene StandardAnalyzer.
 *
 * @author Robert Burke
 */
@ResourceMetaData(
        name = "Lucene Standard Analyzer",
        description = "Tokenizes a document using the Apache Lucene StandardAnalyzer",
        vendor = "Apache",
        version = "4.7.0",
        copyright = "Apache"
)
@OperationalProperties(
        modifiesCas = true,
        outputsNewCases = false
)
@TypeCapability(outputs = {"com.deep6.uima.annotations.Token"})
public class LuceneTokenizerAnnotator extends JCasAnnotator_ImplBase {
    private static final Logger log = LoggerFactory.getLogger(LuceneTokenizerAnnotator.class);


    public static final String PARAM_STEM = "stemming";
    @ConfigurationParameter(name = PARAM_STEM, description = "Stems tokens using the Porter Stemmer", mandatory = false, defaultValue = {"true"})
    private boolean stemming = true;

    public static final String PARAM_CASE_SENSTIVE = "case_sensitive";
    @ConfigurationParameter(name=PARAM_CASE_SENSTIVE, description = "Boolean true = case senstive", mandatory = false, defaultValue = {"false"})
    private boolean case_sensitive = false;

    private StandardAnalyzer analyzer;

    private PorterStemmer stemmer;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);

        analyzer = new StandardAnalyzer(Version.LUCENE_36);

        stemmer = new PorterStemmer();
    }

    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {
        String content = jCas.getDocumentText();
        // log.info(content);
        StringReader reader = new StringReader(content);

        try {
            TokenStream stream  = analyzer.tokenStream(null, reader);
            OffsetAttribute offset = stream.getAttribute(OffsetAttribute.class);
            stream.reset();

            while (stream.incrementToken()) {
                Token token = new Token(jCas);
                token.setBegin(offset.startOffset());
                token.setEnd(offset.endOffset());

                if (isStemming()) {
                    stemmer.setCurrent(token.getCoveredText());
                    stemmer.stem();
                }
                if (isCaseSensitive()) {
                    token.setStem(stemmer.getCurrent());
                } else {
                    token.setStem((stemmer.getCurrent().toLowerCase()));
                }
                token.addToIndexes();
            }

            stream.end();
            stream.close();
        } catch (IOException e) {
            throw new AnalysisEngineProcessException(e);
        }

    }

    public boolean isStemming() {
        return stemming;
    }

    public void setStemming(boolean stemming) {
        this.stemming = stemming;
    }

    public boolean isCaseSensitive() {return case_sensitive;}

    public void setCaseSensitive(boolean cs) {
        this.case_sensitive = cs;
    }
}
